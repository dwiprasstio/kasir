<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Account extends CI_Controller {
	public function __construct(){
		parent::__construct();
		$this->load->helper('html');
		$this->load->helper('form');
		$this->load->helper('url');
		$this->load->model('user_model');
	}
	public function login(){
		$this->load->view('account/login');
	}
	public function process_login(){
		$username = $this->input->post('username');
		$password = $this->input->post('password');

		$validate = $this->user_model->login($username , $password);

		if($validate == true){
			$data = array(
				'login' => true,
				'username' => $username);
			$this->session->set_userdata($data);
			redirect('home');
		}else {
			$this->session->set_flashdata('login_gagal', 'username atau password salah');
			redirect('account/login');
		}
	}
	public function logout(){
		$this->session->set_userdata('login', false);
		redirect('account/login');
	}
}
