<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Example extends CI_Controller {

	public function __construct(){
		parent::__construct();
		$this->load->model('example_model');
	}

	public function create($name = null , $type = null , $price = null){
		if($name != null && $type != null && $price != null){
			if(is_numeric($price))
			{
			$data = array(
					'name' => urldecode($name),
					'type' => $type,
					'price' => $price
					);	

			$this->menu_model->create($data);
			echo 'data berhasil disimpan';
			}else {
				echo 'harga harus berupa angka';
			}
		} else{
			echo 'data harus di isi';
		}
	}

	public function read($id = null){

		

			if($id == null){

				$menus = $this->menu_model->read();
				foreach ($menus as $menu) {
					echo 'name :'. $menu->name . '<br />';
				}
			} else{
				if(is_numeric($id)){
				$menu = $this->menu_model->read($id);
				echo $menu->name;				
				} else{
					echo 'id harus berupa angka';
				}
			}
		
	}

	public function update($id){

		$data = array(
			'name'=> 'teh tarik manis',
			'type'=> 'drinks'
			);

		$this->menu_model->update($id , $data);
	}

	public function delete($id){
		if($id != null){
			if(is_numeric($id)){
				$this->menu_model->delete($id);
			}else {
				echo 'id harus angka';
			}
		} else{
			echo 'id harus di isi';
		}
	}
}