<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller {
	public function __construct(){
		parent::__construct();
		$this->load->helper('html');
		$this->load->helper('url');
		
		$this->load->helper('gua');
		validate_login();
	}

	public function index(){
		$this->load->view('home/index');
	}
	

}
