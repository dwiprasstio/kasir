<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Menu extends CI_Controller {
	public function __construct(){
		parent::__construct();
		$this->load->model('menu_model');
		$this->load->helper('html');
		$this->load->helper('url');
		
		$this->load->helper('gua');
		validate_login();
	}

	public function index(){
		$food = $this->menu_model->get_by_type('food');
		$drinks = $this->menu_model->get_by_type('drinks');
		$data = array(
			'foods' => $food , 
			'drinks' => $drinks);
		$this->load->view('menu/index', $data);
	}
	
	public function delete($id){
		$menu = $this->menu_model->get_by_id($id);

		if($menu) {
			$this->menu_model->delete($id);
			unlink('./assets/images/'.$menu->images);
			$this->session->set_flashdata('message', 'item berhasil dihapus');
		} else{
			$this->session->set_flashdata('message', 'item tidak ditemukan');
		}
			redirect ('menu');
	}

	public function add(){
		$this->load->view('menu/add');
	}

	public function update($id = null){
		if($id == null)
			redirect ('menu/index');

		$menu = $this->menu_model->get_by_id($id);
		
		if($menu == null)
			redirect ('menu/index');
			
		$data = array(
					'name' => $menu->name , 
					'type' => $menu->type ,
					'price' => $menu->price ,
					'images' => $menu->images,
					'id' => $id
					);
		$this->load->view('menu/update', $data);
	}


	public function process_add(){
		$config['upload_path'] = './assets/images/';
		$config['allowed_types'] = 'gif|jpeg|jpg|png';
		$config['max_size'] = 2000;

		$this->load->library('upload', $config);

		if(!$this->upload->do_upload('menu_image')){
			$error = $this->upload->display_errors();
			$this->session->set_flashdata('upload_error' , $error);
			redirect('menu/add');
		} else {
			$upload_data = $this->upload->data();
			$image_name = $upload_data['file_name'];
		}

		$name = $this->input->post('menu_name');
		$type = $this->input->post('menu_type');
		$price = $this->input->post('menu_price');

		$data = array(
			'name' => $name,
			'type' => $type,
			'price' => $price,
			'images' => $image_name);

		$this->menu_model->add($data);
		$this->session->set_flashdata('message', $name . ' berhasil ditambah');
		redirect('menu');
	}	

	public function process_update($id = null){
		if($id == null)
			redirect('menu/index');
		
		$menu = $this->menu_model->get_by_id($id);
		if($menu == null)
			redirect('menu/index');

		$name = $this->input->post('menu_name');
		$type = $this->input->post('menu_type');
		$price= $this->input->post('menu_price');

		$data = array(
				'name' => $name , 
				'type' => $type ,
				'price'=> $price,
			);
		
		if(!empty($_FILES['menu_images']['name'])){
        	$config['upload_path'] = './assets/images/';
	        $config['allowed_types'] = 'gif|jpg|jpeg|png';
	        $config['max_size'] = 2000;

	        $this->load->library('upload', $config);

	        if (!$this->upload->do_upload('menu_image')) {
	        	$error = $this->upload->display_errors();
	        	$this->session->set_flashdata('upload_error', $error);
	            redirect('menu/update');
	        } else {
	            $upload_data = $this->upload->data();
	            $image_name = $upload_data['file_name'];

	            $data['images'] = $image_name;
	            unlink('./assets/images/'.$menu->images);
	        }
        }

        $this->menu_model->update($id, $data);

        $this->session->set_flashdata('message', 'Menu berhasil diubah');
    	redirect('menu');

	}

	
}
