<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Table extends CI_Controller {
	public function __construct(){
		parent::__construct();
		$this->load->model('table_model');
		$this->load->helper('html');
		$this->load->helper('url');
		
		$this->load->helper('gua');
		validate_login();
	}

	public function index(){

		$tables = $this->table_model->get_all();
		$data = array(
				'tables' => $tables
			);

		$this->load->view('table/index', $data);
	}
	public function add(){
		$get_biggest = $this->table_model->get_biggest_table_number();
		$max_number = $get_biggest->table_number;

		$new_number = $max_number + 1;

		$data_insert= array(
				'table_number' => $new_number,
				'available' => 0
			);
		$this->table_model->add($data_insert);

		$this->session->set_flashdata('message', 'table berhasil ditambah');
		redirect('table');
	}
	public function remove(){

	}
}