<?php
defined('BASEPATH') OR exit('No direct script access allowed');

if(!function_exists('validate_login')){
	function validate_login(){
		$ci =& get_instance();
		if(!$ci->session->login){
			redirect('account/login');
		}
	}
}