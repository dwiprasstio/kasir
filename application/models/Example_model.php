<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Menu_model extends CI_Model {

	public function __construct(){
			parent::__construct();
		}
	public function create($data){
		$this->db->insert('menus', $data);
	}
	public function read($id = null){
		if($id == null){
			return $this->db->get('menus')->result();
		} else{
			$this->db->where('id', $id);
			return $this->db->get('menus')->first_row();
		}
	}
	public function update($id , $d){
		$this->db->where('id' , $id);
		$this->db->update('menus', $d);
	}
	public function delete($id){
		$this->db->where('id', $id);
		$this->db->delete('menus');
	}

}