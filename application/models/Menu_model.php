<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Menu_model extends CI_Model {

	public function __construct(){
			parent::__construct();
		}

	public function get_by_id($id){
		$this->db->where('id' , $id);
		$result = $this->db->get('menus')->first_row();
		return $result;
	}
	public function get_by_type($type){
		$this->db->where('type' , $type);
		$result = $this->db->get('menus')->result();
		return $result;
	}
	public function delete($id){
		$this->db->where('id' , $id);
		$this->db->delete('menus');
	}

	public function add($data){
		$this->db->insert('menus', $data);
	}
	public function update($id, $data) {
        $this->db->where('id', $id);
        $this->db->update('menus', $data);
    }

}