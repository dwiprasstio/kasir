<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Table_model extends CI_Model {

	public function __construct(){
			parent::__construct();
		}
	public function get_all(){
		$result = $this->db->get('tables')->result();
		return $result;
	}
	public function get_biggest_table_number(){
		$this->db->select_max('table_number');
		$result = $this->db->get('tables')->first_row();
		return $result;
	}
	public function add($data){
		$this->db->insert('tables' , $data);
	}
}