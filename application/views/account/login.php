<!DOCTYPE html>
<html>
	<head>
		<title>Login</title>
		<?php echo link_tag('assets/css/style.css')?>
	</head>
	<body>
		<div id="header">
		<p>KasApp</p>
		</div>
		<div id="content">
			<h3>Please Login</h3>
			<div id="login_form">
				<?php echo form_open('account/process_login');?>
				<div id="username">
				<label>username</label>
				<input type="text" name="username">
				</div>
				<div id="password">
				<label>password</label>
				<input type="password" name="password">
				</div>
				<div id="submit_login">
				<input type="submit" name="submit_login" value="login">
				</form>
				<?php
					if( $this->session->flashdata('login_gagal')){
						echo $this->session->flashdata('login_gagal');
					}
				?>
			</div>
	</body>
</html>