<!DOCTYPE html>
<html>
	<head>
		<title>Home</title>
		<?php echo link_tag('assets/css/style.css')?>
	</head>
	<body>
		<div id="header">
		<p>KasApp</p>
		</div>
		<div id="content">
			<p>Welcome,  <?php echo $this->session->username?></p>
			<a href="<?php echo site_url()?>/account/logout">logout</a>
			<h2>Add new menu</h2>
			<div id="cafe-menu">
				<form method="post" action="<?php echo site_url() ?>/menu/process_add" enctype="multipart/form-data">
					<div>
						<label>Name</label>
						<input type="text" name="menu_name"/>
					</div>

					<div>
						<label>type</label>
						<select name="menu_type">
							<option value="food">Food</option>
							<option value="drinks">drink</option>
						</select>
					</div>

					<div>
						<label>price</label>
						<input type="text" name="menu_price"/>
					</div>
					
					<div>
						<label>menu image</label>
						<input type="file" name="menu_image"/>
					</div>

					<div>
						<input type="submit" name="submit" value="save">
					</div>
				</form>
				<div class="error">
					<?php
						if($this->session->flashdata('upload_error')){
							echo $this->session->flashdata('upload_error');
						}
					?>
			</div>
		</div>
	</body>
</html>

