<!DOCTYPE html>
<html>
	<head>
		<title>Home</title>
		<?php echo link_tag('assets/css/style.css')?>
	</head>
	<body>
		<div id="header">
		<p>KasApp</p>
		</div>
		<div id="content">
			<p>Welcome,  <?php echo $this->session->username?></p>
			<a href="<?php echo site_url()?>/account/logout">logout</a>

			<div id="cafe-menu">
				<h2>Cafe Menu</h2>
				<a href="<?php echo site_url()?>/menu/add">add new</a>

				<?php 
					if($this->session->flashdata('message')):
					?>
				<div class="alert">
					<?php echo $this->session->flashdata('message')?>
				</div>
				<?php
					endif;
					?>
				<div class="menu-category">
					<h3>Drinks</h3>
					<div class="menu-item">
						<?php
							foreach ($drinks as $drink): ?>
<!-- 								$menu_name = $drink->name . ' ('.number_format($drink->price).')';
								$delete = '<a href=" '.site_url().'/menu/delete/'.$drink->id.' ">delete</a>';
								echo '<figure>';
								echo '<img src="' .base_url(). 'assets/images/'.$drink->images.'" width="180" height="180"/>';
								echo '<figcaption>' . $menu_name. '</br>'.$delete. '</figcaption>';
								echo '</figure>'; -->
								<figure>
									<img src="<?php echo base_url(). 'assets/images/' . urlencode($drink->images)?>" width="180" height="180">
									<figcaption>
										<?php echo $drink->name ?> (<?php echo (number_format($drink->price))?>)
										<br/>
										<a href="<?php echo site_url(). '/menu/update/' . $drink->id ?>">update</a>
										<a href="<?php echo site_url(). '/menu/delete/' . $drink->id ?>">delete</a>
									</figcaption>
								</figure>
						
					<?php endforeach;?>
					</div>
				</div>
				<div class="menu-category">
					<h3>Food</h3>
					<div class="menu-item">
						<?php
							foreach ($foods as $food) {
								$menu_name = $food->name . ' ('.number_format($food->price).')';
								$update = '<a href=" '.site_url().'/menu/update/'.$food->id.' ">update</a>';
								$delete = '<a href=" '.site_url().'/menu/delete/'.$food->id.' ">delete</a>';
								echo '<figure>';
								echo '<img src="' .base_url(). 'assets/images/'. urlencode($food->images).'" width="180" height="180"/>';
								echo '<figcaption>' . $menu_name. '</br>'.$update.' '.$delete.'</figcaption>';
								echo '</figure>';
							}
						?>
					</div>
				</div>
			</div>
		</div>
	</body>
</html>