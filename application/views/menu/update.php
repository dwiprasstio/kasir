<!DOCTYPE html>
<html>
	<head>
		<title>Home</title>
		<?php echo link_tag('assets/css/style.css')?>
	</head>
	<body>
		<div id="header">
		<p>KasApp</p>
		</div>
		<div id="content">
			<p>Welcome,  <?php echo $this->session->username?></p>
			<a href="<?php echo site_url()?>/account/logout">logout</a>
			<h2>Update menu</h2>
			<div id="cafe-menu">
				<form method="post" action="<?php echo site_url() ?>/menu/process_update/<?php echo $id ?>" enctype="multipart/form-data">
					<div>
						<label>Name</label>
						<input type="text" name="menu_name" value="<?php echo $name ?>"/>
					</div>

					<div>
						<label>type</label>
						<select name="menu_type">
							<option value="food" <?php if($type == 'food') echo 'selected="selected"'?>>Food</option>
							<option value="drinks"<?php if($type == 'drinks') echo 'selected="selected"'?>>drink</option>
						</select>
					</div>

					<div>
						<label>price</label>
						<input type="text" name="menu_price" value="<?php echo $price ?>"/>
					</div>
					
					<div>
						<label>menu image</label>
						<input type="file" name="menu_image"/>
					</div>

					<div>
						<img src="<?php echo base_url(). 'assets/images/' . urlencode($images)?>" width="180" height="180">						
					</div>
					
					<div>
						<input type="submit" name="submit" value="update">
					</div>

					<input type="hidden" name="id" value="<?php echo $id ?>" />

				</form>
				<div class="error">
					<?php
						if($this->session->flashdata('upload_error')){
							echo $this->session->flashdata('upload_error');
						}
					?>
			</div>
		</div>
	</body>
</html>

