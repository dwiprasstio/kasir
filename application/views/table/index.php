<!DOCTYPE html>
<html>
	<head>
		<title>Home</title>
		<?php echo link_tag('assets/css/style.css')?>
	</head>
	<body>
		<div id="header">
		<p>KasApp</p>
		</div>
		<div id="content">
			<p>Welcome,  <?php echo $this->session->username?></p>
			<a href="<?php echo site_url()?>/account/logout">logout</a>
				<?php 
					if($this->session->flashdata('message')):
					?>
				<div class="alert">
					<?php echo $this->session->flashdata('message')?>
				</div>
				<?php
					endif;
					?>

			<div id="Tables">
				<h2>table management</h2>
				<div id="table_action">
					<a href="<?php echo site_url()?>/table/add">add</a>
					<a href="<?php echo site_url()?>/table/remove">remove</a>
				</div>
				<?php
					foreach ($tables as $table) :
				?>
				<div class="tables <?php if($table->available == 1) echo 'not-available'?>">
					<p><?php echo $table->table_number ?></p>
				</div>
				<?php endforeach; ?>
			</div>
		</div>
	</body>
</html>